﻿// Lab2.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

using namespace std;

class Date {
	// дата по умолчанию как статический член класса.
private:
	static const int DEFOULT_DAY = 1;
	static const int DEFOULT_MONTH = 1;
	static const int DEFOULT_YEAR = 2000;
	int currentDay, currentMonth, currentYear;
	bool isLeapYear(int year) {
		return ((year % 4 != 0) || (year % 100 == 0) && (year % 400 != 0));
	}

	int howDaysIn(int month) {
		if (month == 0) month = 12;
		if (month == apr || month == jun || month == sep || month == nov) return 30;
		else if (month == feb) {
			if (isLeapYear(currentYear)) return 28;
			return 29;
		}
		return 31;
	}

public:
	static enum { jan = 1, feb, mar, apr, may, jun, jul, aug, sep, oct, nov, dec };
	//Конструктор по умолчанию
	Date() {
		currentDay = DEFOULT_DAY;
		currentMonth = DEFOULT_MONTH;
		currentYear = DEFOULT_YEAR;
	}
	//Конструктор по Году и(или) месяцу и(или)
	Date(int year, int month = DEFOULT_MONTH, int day = DEFOULT_DAY) {
		currentDay = day;
		currentMonth = month;
		currentYear = year;
	}
	//Конструктор копирования
	Date(const Date& d) {
		currentDay = d.currentDay;
		currentMonth = d.currentMonth;
		currentYear = d.currentYear;
	}
	//Изменения даты на целое количество дней
	void increaseDay(int count) {
		this->currentDay += count;

		int daysInCurrentlyMonth = howDaysIn(currentMonth);

		if (currentDay < 1)
		{
			this->increaseMonth(-1);
			this->increaseDay(howDaysIn(currentMonth));
			return;
		}

		else if (currentDay > daysInCurrentlyMonth) {
			this->increaseMonth(1);
			this->increaseDay(-daysInCurrentlyMonth);
		}

	}
	//Изменения даты на целое количество месяцев
	void increaseMonth(int count) {
		this->currentMonth += count;
		if (currentMonth < 1) {
			this->increaseYear(-1);
			this->increaseMonth(12);
			return;
		}
		if (currentMonth > 12) {
			this->increaseYear(1);
			this->increaseMonth(-12);
		}

	}
	//Изменения даты на целое количество лет
	void increaseYear(int count) {
		this->currentYear += count;
	}

	//Перегрузка оператора (присваивание) =
	Date operator =(Date d) {
		currentDay = d.currentDay;
		currentMonth = d.currentMonth;
		currentYear = d.currentYear;
		return *this;
	}
	//Перегрузка оператора +=
	Date operator +=(int n) {
		increaseDay(n);
		return *this;
	}
	//Перегрузка оператора -=
	Date operator -=(int n) {
		increaseDay(-n);
		return *this;
	}
	//Перегрузка оператора +
	Date operator+(const int i) {
		Date tmp(*this);
		tmp.increaseDay(i);
		return tmp;
	}
	//Перегрузка оператора -
	Date operator-(const int i) {
		Date tmp(*this);
		tmp.increaseDay(-i);
		return tmp;
	}
	// ++date (префиксный оператор)
	Date& operator ++() {
		increaseDay(1);

		return *this;
	}
	// date++ (постфиксный оператор)
	Date operator ++(int) {
		Date tmp(*this);
		++(*this);
		return tmp;
	}
	// --date (префиксный оператор)
	Date& operator --() {
		increaseDay(-1);

		return *this;
	}
	// date-- (постфиксный оператор)
	Date operator --(int) {
		Date tmp(*this);
		--(*this);
		return tmp;
	}
	//Перегрузка (операторы сравнения) ==, !=, >, <
	friend bool operator ==(const Date& d1, const Date& d2);
	friend bool operator !=(const Date& d1, const Date& d2);
	friend bool operator >(const Date& d1, const Date& d2);
	friend bool operator <(const Date& d1, const Date& d2);
	//Перегрузка >> << (ввод/вывод с помощью потоков типа istream/ostream)
	friend ostream& operator<<(ostream& out, const Date& d);
	friend istream& operator>> (istream& in, const Date& d);

	//запроса года, месяца, дня.
	int getDay() {
		return currentDay;
	}
	int getMonth() {
		return currentMonth;
	}
	int getYear() {
		return currentYear;
	}
};


ostream& operator<<(ostream& out, const Date& d) {
	out << d.currentDay << "/" << d.currentMonth << "/" << d.currentYear << "\t";
	return out;
}
istream& operator>> (istream& in, Date& d){
	int count;
	in >> count;
	d.increaseDay(count-d.getDay());
	in >> count;
	d.increaseMonth(count - d.getMonth());
	in >> count;
	d.increaseYear(count - d.getYear());
	return in;
}
bool operator ==(const Date& d1, const Date& d2) {
	return (d1.currentDay == d2.currentDay &&
		d1.currentMonth == d2.currentMonth &&
		d1.currentYear == d2.currentYear);
}
bool operator !=(const Date& d1, const Date& d2) {
	return !(d1.currentDay == d2.currentDay &&
		d1.currentMonth == d2.currentMonth &&
		d1.currentYear == d2.currentYear);
}
bool operator >(const Date& d1, const Date& d2) {
	return (d1.currentDay > d2.currentDay ||
		d1.currentMonth > d2.currentMonth ||
		d1.currentYear > d2.currentYear);
}
bool operator <(const Date& d1, const Date& d2) {
	return (d1.currentDay < d2.currentDay ||
		d1.currentMonth < d2.currentMonth ||
		d1.currentYear < d2.currentYear);
}



int main()
{
	Date data;
	cin >> data;
	cout << data << endl;
	return 0;
}

